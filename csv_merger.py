import os
import errno


class CsvMerger(object):
    def __init__(self, input_folder, output_file_path):
        self.input_folder = input_folder
        self.output_file_path = output_file_path

    def merge_files(self):
        all_input_files = os.listdir(self.input_folder)

        # Making sure output file exists, if no - creating a new one
        if not os.path.exists(os.path.dirname(self.output_file_path)):
            self._make_new_file(self.output_file_path)
        else:
            if os.path.exists(self.output_file_path):
                print("Removing file " + self.output_file_path)
                os.remove(self.output_file_path)
                self._make_new_file(self.output_file_path)
            else:
                print("Previous summary report doesn't exist, creating new report")

        output_file = open(self.output_file_path, 'a')

        header_inserted = False
        for file_name in all_input_files:
            if file_name.endswith('.csv'):
                print("Merging file " + file_name + " into " + self.output_file_path)
                f = open(self.input_folder + '\\' + file_name)
                if not header_inserted:
                    f.__next__()
                    header_inserted = True
                else:
                    f.__next__()
                    f.__next__()
                for line in f:
                    output_file.write(line)
                f.close()
        output_file.close()

    def _make_new_file(self, path):
        try:
            print("Creating a new file " + path)
            os.makedirs(os.path.dirname(path))
        except OSError as exc:  # Guard against race condition
            if exc.errno != errno.EEXIST:
                raise
