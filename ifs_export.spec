# -*- mode: python ; coding: utf-8 -*-

block_cipher = None


a = Analysis(['core\\export.py'],
             pathex=['C:\\_projects\\ifs_export'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='ifs_export.exe',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          upx_exclude=[],
          runtime_tmpdir=None,
          console=False )

import shutil
shutil.copyfile('config.py', '{0}/config.py'.format(DISTPATH))
shutil.copyfile('README.txt', '{0}/README.txt'.format(DISTPATH))
shutil.copyfile('evry_ids.csv', '{0}/evry_ids.csv'.format(DISTPATH))

