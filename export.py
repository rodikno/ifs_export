from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from helpers import *
import pprint
from selenium.webdriver.support.ui import Select
from config import *
import json
from csv_merger import CsvMerger
from time import sleep

pp = pprint.PrettyPrinter(indent=4)
options = Options()
options.add_experimental_option("prefs", {
    "download.default_directory"  : DOWNLOADS_DIR,
    "download.prompt_for_download": False,
    "download.directory_upgrade"  : True,
    "safebrowsing.enabled"        : True
})
options.add_extension(AUTH_EXTENSION_PATH)
options.add_argument("--no-sandbox")

driver = webdriver.Chrome(options=options)
driver.get(CONNECTION_URI)
sleep(5)
driver.maximize_window()

remove_dir_with_files(DOWNLOADS_DIR)

evry_ids_dict = import_csv_file(INPUT_FILE_DIR)

# Company select values we are interested in
# 40 -> Evry Norge AS
driver.find_element_by_name('date_start').send_keys(START_DATE)
driver.find_element_by_name('date_end').send_keys(END_DATE)

company_select = Select(driver.find_element_by_name('company_id'))

# Download reports for main Company
current_company_id = EVRY_NORGE_AS
company_select.select_by_value(current_company_id)
missed_employee_names = download_reports_for_dict(driver, evry_ids_dict, current_company_id)

for company_code in list(OTHER_COMPANIES.values()):
    current_company_id = company_code
    if len(missed_employee_names) > 0:
        for key in list(evry_ids_dict.keys()):
            if key not in missed_employee_names:
                evry_ids_dict.pop(key)
        company_select = Select(driver.find_element_by_name('company_id'))
        company_select.select_by_value(current_company_id)
        download_reports_for_dict(driver, evry_ids_dict, current_company_id)

# TODO Create a file in output folder with list of all missed employee names (to see who is not present in IFS for both companies
if len(missed_employee_names) > 0:
    print("The following pairs do not have reports available:\n")
    pp.pprint(evry_ids_dict)
    if os.path.exists(OUTPUT_MISSING_REPORTS_FILE):
        print("Removing previous missing reports file " + OUTPUT_MISSING_REPORTS_FILE)
        os.remove(OUTPUT_MISSING_REPORTS_FILE)
    with open(OUTPUT_MISSING_REPORTS_FILE, "w") as file:
        file.write(json.dumps(evry_ids_dict))


driver.quit()

print("====== MERGING REPORTS INTO ONE =======")
merger = CsvMerger(DOWNLOADS_DIR, OUTPUT_MERGED_CSV_FILE_PATH)
merger.merge_files()
print("====== REPORTS MERGE COMPLETE =========")
