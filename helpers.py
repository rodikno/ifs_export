from time import sleep
import os
import shutil
import logging
import pandas
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains

from config import *


def wait_until_file_exists(path_to_file):
    while not os.path.exists(path_to_file):
        sleep(1)


def remove_dir_with_files(path_to_dir):
    if os.path.exists(path_to_dir):
        logging.info("Removing directory " + path_to_dir)
        shutil.rmtree(path_to_dir)


def import_csv_file(path_to_file):
    df = pandas.DataFrame
    if os.path.exists(path_to_file):
        df = pandas.read_csv(path_to_file, header=0, delimiter=';', index_col=1, skip_blank_lines=True).to_dict().get(
            'EVRY ID')
    return df


def download_reports_for_dict(driver, input_dict, company_id):
    missed_names = []
    for name, evry_id in input_dict.items():
        print(f'\nGetting report for [{name}] for company ID [{company_id}]:')

        # if ((len(evry_id) < 1) or evry_id == 'nan'):
        #     print(f'Evry id is missing for employee {name}')
        #     missed_names.append(name)

        filter_res = "".join(filter(lambda x: x.isdigit(), evry_id))
        evry_id = filter_res

        # ORG Code
        driver.find_element_by_name('employee_no').clear()
        driver.find_element_by_name('employee_no').send_keys(evry_id)
        driver.find_element_by_name('Send').click()

        try:
            download_link = driver.find_element_by_css_selector("a[href*='reports']")
            file_name = download_link.get_attribute('href').rsplit('/', 1)[1]
            print("Downloading file " + file_name)
            download_link.click()
            wait_until_file_exists(DOWNLOADS_DIR + "\\" + file_name)
        except NoSuchElementException:
            print(f'Report not found for {evry_id} (employee {name})')
            missed_names.append(name)
            continue
    return missed_names
