from pathlib import Path


def get_project_root() -> str:
    """Returns project root folder."""
    return str(Path(__file__).parent)


ROOT_DIR = get_project_root()
DOWNLOADS_DIR = ROOT_DIR + r"\downloads"
AUTH_EXTENSION_PATH = ROOT_DIR + r"\auth_extension\auth_extension.crx"
INPUT_FILE_DIR = ROOT_DIR + r"\input\evry_ids.csv"
OUTPUT_MERGED_FILES_DIR = ROOT_DIR + r"\output"
OUTPUT_MISSING_REPORTS_FILE = ROOT_DIR + r"\output\missing_reports.txt"
OUTPUT_MERGED_CSV_FILE_PATH = OUTPUT_MERGED_FILES_DIR + r"\ifs_report.csv"

EVRY_NORGE_AS = '40'

OTHER_COMPANIES = {
    'EVRY_CARD_ISSUING': '35',
    'EVRY_CARD_SERVICES_AS': '41',
    'EVRY_LATVIA_SIA': '123',
    'EYE_SHARE_AS': '52',
    'GJELDSREGISTERET_AS': '28'
}

# PLEASE EDIT THESE VALUES WHEN YOU WANT TO SELECT ANOTHER DATES OR CREDENTIALS
USERNAME = 'E218295'
PASSWORD = 'K@tya1991'
START_DATE = "01.03.2021"
END_DATE = "21.03.2021"
# CONNECTION_URI = 'https://{}:{}@fmsapps.evry.com/IfsReports/ProjectAnalysis/'.format(USERNAME, PASSWORD)
CONNECTION_URI = 'https://fmsapps.evry.com/IfsReports/ProjectAnalysis/'
