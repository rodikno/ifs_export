## Description
This script allows you to export IFS reports for a set of selected employees.
It accepts .csv file with a specific format which contains a list of EVRY IDs, uses an 'IFS Project Analysis' web tool to automate routine operations for each ID using Selenium.
As a result -> you will get a compiled summarized report of reported IFS records for all employees who has reported something in an output CSV file.
Additionally, it tracks for which users reports weren't found and stores a list of Names and EVRY IDs in a file `no_reports.txt` in the output folde

## Setup and preparation
Steps below require some basic knowledge of Git VCS, managing `PATH` system variable and basic command line skills.
You also should have `Google Chrome`, `Python 3.X` and `Git` installed to your machine.
If you feel there're some scary abbreviations in the list -> please Google it or contact someone who can explain it in short (this will be useful to learn anyway, so now it the best moment to start :)
1) Clone this repository to any folder you want, but trying to keep path in `latin` alphabet and better `C:` drive on Windows machine (to avoid file access restrictions)

        # Go to your system drive root
        cd C:\
        # Create new folder called ifs_export
        mkdir ifs_export
        # Clone repository from Bitbucket using Git
        git clone git@bitbucket.org:rodikno/ifs_export.git

2) Download latest `Chromedriver` from https://chromedriver.chromium.org/downloads , depending on your Chrome version (you can check version going to `Help -> About Google Chrome`
3) Extract `chromedriver.exe` and put it somewhere neat, like `C:\_executables\chromedriver` folder
4) Add folder from step #3 to your PATH -> google it if you don't know how to do it
5) After Chromedriver is added to PATH, run a new `cmd` and execute:

        C:\Users\Your.User>chromedriver
    If you're getting output like below -> than it's ok:

        Starting ChromeDriver 78.0.3904.105 (60e2d8774a8151efa6a00b1f358371b1e0e07ee2-refs/branch-heads/3904@{#877}) on port 9515
        Only local connections are allowed.
        Please protect ports used by ChromeDriver and related test frameworks to prevent access by malicious code.

6) Cd to your `ifs_export` folder and run:

        `python pip install -r requirements.txt` -> this will automatically install all required packages

7) Configure your script by using instructions from `Configuration` section below
8) Run export using `python export.py` or by running `run_export.cmd` from the project folder



## Configuration
1) You can specify the input list of employees in `/input/evry_ids.csv` (please be careful and keep it in CSV format, otherwide script will fail)
2) You can select the export dates by editing file `config.py`

    Just edit the following variables:

        START_DATE = "01.01.2020" -> put your start date here
        END_DATE = "26.01.2020" -> put your end date here

3) Your /CORP credentials are required to open the IFS reporting tool main page. Recent changes in Google Chrome required to add a specific extension to pass an authentification through alert:

https://stackoverflow.com/questions/42114940/how-to-handle-authentication-popup-in-chrome-with-selenium-webdriver-using-java#

https://stackoverflow.com/questions/16612968/chrome-webrequest-onauthrequired-listener

https://stackoverflow.com/questions/48757424/runtime-error-on-chrome-extension
        
        USERNAME = 'E123456' -> put your EVRY ID here (check you have access to IFS Project Analysis tool before
        PASSWORD = 'Pass123' -> put your CORP password here (the same you're using for EVRY mail)


## Execution results and output
After the script is complete, you can find results in /output folder

1) File `output/ifs_report.csv` will contain compiled report

2) File `output/no_reports.txt` will contain info about people for whom reports were not found